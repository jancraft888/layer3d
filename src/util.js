export const RAD2DEG = 180 / Math.PI
export const DEG2RAD = Math.PI / 180
export const TOP_VIEW_ANGLE = RAD2DEG

export const matid2x2 = () => [ [ 1, 0 ], [ 0, 1 ] ]
export const matid3x3 = () => [ [ 1, 0, 0 ], [ 0, 1, 0 ], [ 0, 0, 1 ] ]

// WARN: creates an identity matrix for *square* matrices only,
//  otherwise it creates zero-filled matrices
export function matidNxM(rows, cols) {
  const mat = new Array(rows)
  for (let i = 0; i < rows; i++) {
    mat[i] = new Array(cols).fill(0)
    if (rows == cols) mat[i][i] = 1
  }
  return mat
}

export function matrot2x2(m, a) {
  const rotm = [ [ Math.cos(a), -Math.sin(a) ], [ Math.sin(a), Math.cos(a) ] ]
  return matmul(m, rotm)
}

// https://stackoverflow.com/a/10896904
export function transformInvertMatrix(dommat) {
  const m = matid3x3()

  const M = (dommat.a * dommat.d) - (dommat.b * dommat.c)
  m[0][0] = dommat.d / M
  m[0][1] = -dommat.c / M
  m[0][2] = (dommat.c * dommat.f - dommat.d * dommat.e) / M
  m[1][0] = -dommat.b / M
  m[1][1] = dommat.a / M
  m[1][2] = (dommat.b * dommat.e - dommat.a * dommat.f) / M

  return m
}

export function matmul(a, b) {
  const rmat = matidNxM(a.length, b[0].length)
  return rmat.map((row, i) => {
    return row.map((val, j) => {
      return a[i].reduce((sum, elm, k) => sum + (elm * b[k][j]), 0)
    })
  })
}

export const matmulvec2x2 = (m, v) => [
  m[0][0] * v[0] + m[0][1] * v[1],
  m[1][0] * v[0] + m[1][1] * v[1],
]

export const matmulvec3x3 = (m, v) => [
  m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2],
  m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2],
  m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2],
]

export const randvec3 = () => [ Math.random() * 2 - 1, Math.random() * 2 - 1, Math.random() * 2 - 1 ]

export function normvec(v) {
  const len = Math.sqrt(v.reduce((p, c) => p + c * c, 0))
  return v.map(x => x / len)
}

export const m4 = {
  translation(tx, ty, tz) {
    return new Float32Array([
      1,  0,  0,  0,
      0,  1,  0,  0,
      0,  0,  1,  0,
      tx, ty, tz, 1,
    ])
  },
  scaling(sx, sy, sz) {
    return new Float32Array([
      sx, 0, 0, 0,
      0, sy, 0, 0,
      0, 0, sz, 0,
      0, 0, 0, 1,
    ])
  },
  rotationX(a) {
    const c = Math.cos(a)
    const s = Math.sin(a)
    return new Float32Array([
      1, 0, 0, 0,
      0, c, s, 0,
      0, -s, c, 0,
      0, 0, 0, 1,
    ])
  },
  rotationY(a) {
    const c = Math.cos(a)
    const s = Math.sin(a)
    return new Float32Array([
      c, 0, -s, 0,
      0, 1, 0, 0,
      s, 0, c, 0,
      0, 0, 0, 1,
    ])
  },
  rotationZ(a) {
    const c = Math.cos(a)
    const s = Math.sin(a)
    return new Float32Array([
      c, s, 0, 0,
      -s, c, 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 1,
    ])
  },
  multiply(a, b) {
    let b00 = b[0 * 4 + 0]
    let b01 = b[0 * 4 + 1]
    let b02 = b[0 * 4 + 2]
    let b03 = b[0 * 4 + 3]
    let b10 = b[1 * 4 + 0]
    let b11 = b[1 * 4 + 1]
    let b12 = b[1 * 4 + 2]
    let b13 = b[1 * 4 + 3]
    let b20 = b[2 * 4 + 0]
    let b21 = b[2 * 4 + 1]
    let b22 = b[2 * 4 + 2]
    let b23 = b[2 * 4 + 3]
    let b30 = b[3 * 4 + 0]
    let b31 = b[3 * 4 + 1]
    let b32 = b[3 * 4 + 2]
    let b33 = b[3 * 4 + 3]
    let a00 = a[0 * 4 + 0]
    let a01 = a[0 * 4 + 1]
    let a02 = a[0 * 4 + 2]
    let a03 = a[0 * 4 + 3]
    let a10 = a[1 * 4 + 0]
    let a11 = a[1 * 4 + 1]
    let a12 = a[1 * 4 + 2]
    let a13 = a[1 * 4 + 3]
    let a20 = a[2 * 4 + 0]
    let a21 = a[2 * 4 + 1]
    let a22 = a[2 * 4 + 2]
    let a23 = a[2 * 4 + 3]
    let a30 = a[3 * 4 + 0]
    let a31 = a[3 * 4 + 1]
    let a32 = a[3 * 4 + 2]
    let a33 = a[3 * 4 + 3]
    return new Float32Array([
      b00 * a00 + b01 * a10 + b02 * a20 + b03 * a30,
      b00 * a01 + b01 * a11 + b02 * a21 + b03 * a31,
      b00 * a02 + b01 * a12 + b02 * a22 + b03 * a32,
      b00 * a03 + b01 * a13 + b02 * a23 + b03 * a33,
      b10 * a00 + b11 * a10 + b12 * a20 + b13 * a30,
      b10 * a01 + b11 * a11 + b12 * a21 + b13 * a31,
      b10 * a02 + b11 * a12 + b12 * a22 + b13 * a32,
      b10 * a03 + b11 * a13 + b12 * a23 + b13 * a33,
      b20 * a00 + b21 * a10 + b22 * a20 + b23 * a30,
      b20 * a01 + b21 * a11 + b22 * a21 + b23 * a31,
      b20 * a02 + b21 * a12 + b22 * a22 + b23 * a32,
      b20 * a03 + b21 * a13 + b22 * a23 + b23 * a33,
      b30 * a00 + b31 * a10 + b32 * a20 + b33 * a30,
      b30 * a01 + b31 * a11 + b32 * a21 + b33 * a31,
      b30 * a02 + b31 * a12 + b32 * a22 + b33 * a32,
      b30 * a03 + b31 * a13 + b32 * a23 + b33 * a33,
    ])
  },
  translate(m, tx, ty, tz) {
    return m4.multiply(m, m4.translation(tx, ty, tz))
  },
  scale(m, sx, sy, sz) {
    return m4.multiply(m, m4.scaling(sx, sy, sz))
  },
  rotateX(m, a) {
    return m4.multiply(m, m4.rotationX(a))
  },
  rotateY(m, a) {
    return m4.multiply(m, m4.rotationY(a))
  },
  rotateZ(m, a) {
    return m4.multiply(m, m4.rotationZ(a))
  },
  perspective(fov, aspectRatio, near, far) {
    const f = 1.0 / Math.tan(fov / 2)
    const rInv = 1 / (near - far)
    return new Float32Array([
      f / aspectRatio, 0, 0, 0,
      0, f, 0, 0,
      0, 0, (near + far) * rInv, -1,
      0, 0, near * far * rInv * 2, 0,
    ])
  },
  identity() {
    return new Float32Array([
      1, 0, 0, 0,
      0, 1, 0, 0,
      0, 0, 1, 0,
      0, 0, 0, 1,
    ])
  },
  inverse(m) {
    // https://github.com/mrdoob/three.js/blob/master/src/math/Matrix4.js
    const result = m4.identity()

    const n11 = m[0], n12 = m[4], n13 = m[8], n14 = m[12]
    const n21 = m[1], n22 = m[5], n23 = m[9], n24 = m[13]
    const n31 = m[2], n32 = m[6], n33 = m[10], n34 = m[14]
    const n41 = m[3], n42 = m[7], n43 = m[11], n44 = m[15]

    result[0] = n23 * n34 * n42 - n24 * n33 * n42 + n24 * n32 * n43 - n22 * n34 * n43 - n23 * n32 * n44 + n22 * n33 * n44
    result[4] = n14 * n33 * n42 - n13 * n34 * n42 - n14 * n32 * n43 + n12 * n34 * n43 + n13 * n32 * n44 - n12 * n33 * n44
    result[8] = n13 * n24 * n42 - n14 * n23 * n42 + n14 * n22 * n43 - n12 * n24 * n43 - n13 * n22 * n44 + n12 * n23 * n44
    result[12] = n14 * n23 * n32 - n13 * n24 * n32 - n14 * n22 * n33 + n12 * n24 * n33 + n13 * n22 * n34 - n12 * n23 * n34
    result[1] = n24 * n33 * n41 - n23 * n34 * n41 - n24 * n31 * n43 + n21 * n34 * n43 + n23 * n31 * n44 - n21 * n33 * n44
    result[5] = n13 * n34 * n41 - n14 * n33 * n41 + n14 * n31 * n43 - n11 * n34 * n43 - n13 * n31 * n44 + n11 * n33 * n44
    result[9] = n14 * n23 * n41 - n13 * n24 * n41 - n14 * n21 * n43 + n11 * n24 * n43 + n13 * n21 * n44 - n11 * n23 * n44
    result[13] = n13 * n24 * n31 - n14 * n23 * n31 + n14 * n21 * n33 - n11 * n24 * n33 - n13 * n21 * n34 + n11 * n23 * n34
    result[2] = n22 * n34 * n41 - n24 * n32 * n41 + n24 * n31 * n42 - n21 * n34 * n42 - n22 * n31 * n44 + n21 * n32 * n44
    result[6] = n14 * n32 * n41 - n12 * n34 * n41 - n14 * n31 * n42 + n11 * n34 * n42 + n12 * n31 * n44 - n11 * n32 * n44
    result[10] = n12 * n24 * n41 - n14 * n22 * n41 + n14 * n21 * n42 - n11 * n24 * n42 - n12 * n21 * n44 + n11 * n22 * n44
    result[14] = n14 * n22 * n31 - n12 * n24 * n31 - n14 * n21 * n32 + n11 * n24 * n32 + n12 * n21 * n34 - n11 * n22 * n34
    result[3] = n23 * n32 * n41 - n22 * n33 * n41 - n23 * n31 * n42 + n21 * n33 * n42 + n22 * n31 * n43 - n21 * n32 * n43
    result[7] = n12 * n33 * n41 - n13 * n32 * n41 + n13 * n31 * n42 - n11 * n33 * n42 - n12 * n31 * n43 + n11 * n32 * n43
    result[11] = n13 * n22 * n41 - n12 * n23 * n41 - n13 * n21 * n42 + n11 * n23 * n42 + n12 * n21 * n43 - n11 * n22 * n43
    result[15] = n12 * n23 * n31 - n13 * n22 * n31 + n13 * n21 * n32 - n11 * n23 * n32 - n12 * n21 * n33 + n11 * n22 * n33
    const determinant = n11 * result[0] + n21 * result[4] + n31 * result[8] + n41 * result[12]	
    for (let i = 0; i < result.length; i++) {
      result[i] /= determinant
    }
    return result
  }
}
