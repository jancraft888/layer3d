export function NewEntity() {
  return { x: 0, y: 0, z: 0, r: 0, typ: 'none', interval: 1, s: 32, shadowoffset: 0 }
}
export function NewEntity3D() {
  return { x: 0, y: 0, z: 0, rx: 0, ry: 0, rz: 0, typ: 'none', sx: 1, sy: 1, sz: 1 }
}

export class ModelUtils {
  static async importFromLMD(url) {
  }

  static buildHeightmapData(fn, w=32, h=32, tall=32, col=x=>[255-x*255,255-x*255,255-x*255]) {
    const layers = []
    for (let i = 0; i < tall; i++) {
      layers.push(new ImageData(w, h))
    }

    for (let y = 0; y < h; y++) {
      for (let x = 0; x < w; x++) {
        const he = Math.floor(fn(x / w, y / h) * (tall-1))
        for (let i = he; i >= 0; i--) {
          const imd = layers[i]
          const idx = (y * w + x) * 4
          const cl = col(i / tall, he / tall, x, y)

          imd.data[idx + 0] = cl[0]
          imd.data[idx + 1] = cl[1]
          imd.data[idx + 2] = cl[2]
          imd.data[idx + 3] = 255
        }
      }
    }

    return layers
  }

  static buildHeightmap(fn, w=32, h=32, tall=32, col=x=>[255-x*255,255-x*255,255-x*255]) {
    const layers = ModelUtils.buildHeightmapData(fn, w, h, tall, col).map(imdat => ({ imdat }))

    for (let i = 0; i < tall; i++) {
      const cnv = new OffscreenCanvas(w, h)
      const ctx = cnv.getContext('2d')
      layers[i].cnv = cnv
      layers[i].ctx = ctx
      ctx.putImageData(layers[i].imdat, 0, 0)
      delete layers[i].imdat
    }

    return {
      ...NewEntity(),
      typ: 'layers',
      imgs: layers,
      w, h,
      edit(layer, fn) {
        this.imgs[layer].imdat = this.imgs[layer].ctx.getImageData(0, 0, w, h)

        fn(this.imgs[layer].imdat)

        this.imgs[layer].ctx.putImageData(this.imgs[layer].imdat, 0, 0)
        delete this.imgs[layer].imdat
        return this
      },
      async permanent() {
        const layers = []

        for (const im of this.imgs) {
          layers.push(await createImageBitmap(im.cnv))
        }

        this.imgs = layers.map(x => ({ cnv: x, w: this.w, h: this.h }))
        return this
      }
    }
  }

  static exportAsLMD(model) {
  }
}

