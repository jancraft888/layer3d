import { Renderer } from './graphics.js'

let scene = []
let canvas = undefined
let renderer = undefined

addEventListener('message', async ev => {
  if (ev.data.taskAction == 'initRender') {
    canvas = ev.data.taskCanvas
    renderer = new Renderer(canvas)
  } else if (ev.data.taskAction == 'start') {
    scene = ev.data.taskData ?? []
    let lasttime = performance.now()
    function animate(now) {
      const delta = (now - lasttime) / 1000
      lasttime = now

      renderer.render(scene)
      requestAnimationFrame(animate)
    }
    animate(lasttime)
  } else if (ev.data.taskAction == 'data') {
    scene = ev.data.taskData ?? []
  } else {
    console.warn(ev.data)
  }
})
