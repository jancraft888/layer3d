let func = undefined

addEventListener('message', async ev => {
  if (ev.data.taskAction == 'initAny') {
    func = eval(ev.data.taskFunction)
  } else if (ev.data.taskAction == 'start') {
    let p = func(ev.data.taskData ?? {})
    if (p instanceof Promise)
      p = await p
    
    postMessage({ taskAction: 'result', taskResult: p })
  } else {
    console.warn(ev.data)
  }
})
