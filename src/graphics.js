import { DEG2RAD, transformInvertMatrix, matmulvec3x3, randvec3, normvec } from './util.js'

export class Renderer {
  constructor(canvas) {
    this.canvas = canvas
    this.ctx = canvas.getContext('2d')
    this.w = canvas.width
    this.h = canvas.height
    this.camera = { x: 0, y: 0, z: 0, s: 1, vangle: 35.0, hangle: 45.0 }
    this.feature = { shadows: false, perspective: this._simple_perspective }
    
    this.ctx.imageSmoothingEnabled = false
    this.canvas.style.imageRendering = 'pixelated'
  }

  detransform(relto, x, y, dx=0, dy=0) {
    const v = matmulvec3x3(relto.matinv, [ x, y, 1 ])
    v[0] /= v[2]
    v[1] /= v[2]
    return [ v[0] + dx, v[1] + dy ]
  }

  _simple_perspective(e, w=0, h=0) {
    this.ctx.translate(0, -e.y)
    this.ctx.scale(1, Math.min(this.camera.vangle * DEG2RAD, 2))
    this.ctx.rotate(this.camera.hangle * DEG2RAD)
    this.ctx.translate(-this.camera.x, this.camera.y - this.camera.z)
    this.ctx.translate(e.x, e.z)
    this.ctx.rotate(e.r * DEG2RAD)
    this.ctx.translate(-w / 2, -h / 2)
    e.matinv = transformInvertMatrix(this.ctx.getTransform())
  }

  _transform_perspective(e, w=0, h=0) {
    const angl = this.camera.vangle * DEG2RAD
    this.ctx.translate(e.x - this.camera.x, -e.y + e.z + this.camera.y - this.camera.z)
    this.ctx.transform(1.0, angl / 2, -1.0, angl / 2, 0, 0)
    this.ctx.rotate(e.r * DEG2RAD)
    this.ctx.translate(-w / 2, -h / 2)
    e.matinv = transformInvertMatrix(this.ctx.getTransform())
  }

  _flat_perspective(e, w=0, h=0) {
    this.ctx.translate(e.x - this.camera.x, -e.y + e.z + this.camera.y - this.camera.z)
    this.ctx.rotate(e.r * DEG2RAD)
    this.ctx.translate(-w / 2, -h / 2)
    e.matinv = transformInvertMatrix(this.ctx.getTransform())
  }

  render(list) {
    this.ctx.clearRect(0, 0, this.w, this.h)
    this.ctx.save()
    this.ctx.translate(this.w / 2, this.h / 2)
    this.ctx.scale(this.camera.s, this.camera.s)

    const perspective = this.feature.perspective.bind(this)

    const rectShadow = (e, w, h) => {
      const xym = Math.cos(this.camera.hangle * DEG2RAD)
      const yym = Math.sin(this.camera.hangle * DEG2RAD)
      this.ctx.rotate(-e.r * DEG2RAD)
      this.ctx.translate(xym * e.y + e.shadowoffset, yym * e.y + e.shadowoffset)
      this.ctx.rotate(e.r * DEG2RAD)
      this.ctx.fillStyle = 'rgba(0,0,0,0.25)'
      this.ctx.fillRect(0, 0, w, h)
      this.ctx.rotate(-e.r * DEG2RAD)
      this.ctx.translate(-xym * e.y - e.shadowoffset, -yym * e.y - e.shadowoffset)
      this.ctx.rotate(e.r * DEG2RAD)
    }
    const circleShadow = (e, r) => {
      this.ctx.rotate(-e.r * DEG2RAD)
      this.ctx.translate(e.y + e.shadowoffset, e.y + e.shadowoffset)
      this.ctx.rotate(e.r * DEG2RAD)
      this.ctx.fillStyle = 'rgba(0,0,0,0.25)'
      fillCircle(0, 0, r)
      this.ctx.rotate(-e.r * DEG2RAD)
      this.ctx.translate(-e.y - e.shadowoffset, -e.y - e.shadowoffset)
      this.ctx.rotate(e.r * DEG2RAD)
    }

    const fillCircle = (x, y, r) => {
      this.ctx.beginPath()
      this.ctx.arc(x, y, r, 0, 2 * Math.PI, false)
      this.ctx.fill()
    }

    for (const entity of list) {
      this.ctx.save()
      if (entity.typ == 'plane') {
        perspective(entity, entity.s, entity.s)
        if (this.feature.shadows && entity.y + entity.shadowoffset > 0) rectShadow(entity, entity.s, entity.s)
        this.ctx.fillStyle = (typeof entity.c == 'function') ? entity.c(0) : (entity.c ?? '#fff')
        this.ctx.fillRect(0, 0, entity.s, entity.s)
      } else if (entity.typ == 'pyramid') {
        for (let i = 0; i < Math.abs(entity.s); i++) {
          this.ctx.save()
          const size = entity.s < 0 ? i : entity.s - i
          perspective(entity, size, size)
          // TODO Fix shadow on inverted 
          if (i == 0 && this.feature.shadows && entity.y + entity.shadowoffset > 0) rectShadow(entity, entity.s, entity.s)
          this.ctx.fillStyle = (typeof entity.c == 'function') ? entity.c(i / (entity.s-1)) : (entity.c ?? '#fff')
          this.ctx.fillRect(0, 0, size, size)
          this.ctx.restore()
          this.ctx.translate(0, -entity.interval)
        }
      } else if (entity.typ == 'cube') {
        for (let i = 0; i < entity.s; i++) {
          this.ctx.save()
          perspective(entity, entity.s, entity.s)
          if (i == 0 && this.feature.shadows && entity.y + entity.shadowoffset > 0) rectShadow(entity, entity.s, entity.s)
          this.ctx.fillStyle = (typeof entity.c == 'function') ? entity.c(i / (entity.s-1)) : (entity.c ?? '#fff')
          this.ctx.fillRect(0, 0, entity.s, entity.s)
          this.ctx.restore()
          this.ctx.translate(0, -entity.interval)
        }
      } else if (entity.typ == 'cylinder') {
        for (let i = 0; i < entity.s; i++) {
          this.ctx.save()
          perspective(entity)
          if (i == 0 && this.feature.shadows && entity.y + entity.shadowoffset > 0) circleShadow(entity, entity.s / 2)
          this.ctx.fillStyle = (typeof entity.c == 'function') ? entity.c(i / (entity.s-1)) : (entity.c ?? '#fff')
          fillCircle(0, 0, entity.s / 2)
          this.ctx.restore()
          this.ctx.translate(0, -entity.interval)
        }
      } else if (entity.typ == 'sphere') {
        for (let i = 0; i < entity.s; i++) {
          this.ctx.save()
          perspective(entity)
          if (i == 0 && this.feature.shadows && entity.y + entity.shadowoffset > 0) circleShadow(entity, entity.s / 2)
          this.ctx.fillStyle = (typeof entity.c == 'function') ? entity.c(i / (entity.s-1)) : (entity.c ?? '#fff')
          const rad = Math.max(Math.sin(i / entity.s * 180.0 * DEG2RAD), 0) * entity.s / 2
          fillCircle(0, 0, rad)
          this.ctx.restore()
          this.ctx.translate(0, -entity.interval)
        }
      } else if (entity.typ == 'layers') {
        for (const im of entity.imgs) {
          this.ctx.save()
          perspective(entity, entity.w, entity.h)
          this.ctx.drawImage(im.cnv, 0, 0)
          this.ctx.restore()
          this.ctx.translate(0, -entity.interval)
        }
      }
      this.ctx.restore()
    }

    this.ctx.restore()
  }
}

export class ParticleRenderer {
  constructor(renderer) {
    this.renderer = renderer
    this.emitters = []
    this.particles = []
    this.lasttime = performance.now()
  }

  render() {
    const delta = (performance.now() - this.lasttime) / 1000
    this.lasttime = performance.now()

    const { w, h, ctx, camera } = this.renderer
    const perspective = this.renderer.feature.perspective.bind(this.renderer)

    ctx.save()
    ctx.translate(w / 2, h / 2)
    ctx.scale(camera.s, camera.s)
    
    for (const emitter of this.emitters) {
      emitter._acc += delta
      emitter.life -= delta
      if (emitter._acc > 1 / emitter.rate) {
        emitter._acc -= 1 / emitter.rate
        const prtcl = Object.assign({}, emitter.particle)
        const pos = randvec3()
        prtcl.x = emitter.x + pos[0] * emitter.ax
        prtcl.y = emitter.y + pos[1] * emitter.ay
        prtcl.z = emitter.z + pos[2] * emitter.az
        this.emit(prtcl, emitter.burst)
      }
    }
    this.emitters = this.emitters.filter(v => v.life > 0)

    let i = 0
    for (const prtcl of this.particles) {
      ctx.save()
      prtcl.size -= delta * prtcl.decay
      prtcl.life -= delta
      prtcl.x += prtcl.vx * delta
      prtcl.y += prtcl.vy * delta
      prtcl.z += prtcl.vz * delta
      prtcl.vy -= prtcl.gravity * delta
      perspective({ x: prtcl.x, y: prtcl.y, z: prtcl.z, s: prtcl.size }, prtcl.size, prtcl.size)
      ctx.fillStyle = (typeof prtcl.color === 'function') ? prtcl.color(prtcl.life) : prtcl.color
      ctx.fillRect(0, 0, prtcl.size, prtcl.size)
      ctx.restore()

      i++
    }
    this.particles = this.particles.filter(v => v.life > 0)
    ctx.restore()
  }

  emit(data={}, count=0) {
    const particle = {
      color: '#fff',
      size: 4,
      x: 0, y: 0, z: 0,
      velocity: 0,
      gravity: 16,
      life: 1,
      ...data
    }
    particle.decay = particle.size / particle.life
    for (let i = 0; i < count; i++) {
      const prtcl = Object.assign({}, particle)
      const vdir = normvec(randvec3())
      prtcl.vx = vdir[0] * particle.velocity
      prtcl.vy = vdir[1] * particle.velocity
      prtcl.vz = vdir[2] * particle.velocity
      this.particles.push(prtcl)
    }
  }

  emitter(data={}) {
    const emitter = {
      x: 0, y: 0, z: 0,
      ax: 0, ay: 0, az: 0,
      particle: {},
      burst: 10,
      rate: .5,
      _acc: 0,
      life: Infinity,
      ...data
    }
    this.emitters.push(emitter)
  }
}
