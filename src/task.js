export class Task {
  constructor(taskfile) {
    this.worker = new Worker(taskfile, { type: 'module' })
    this.result = undefined
  }

  AnyTask(fnc) {
    this.worker.postMessage({ taskAction: 'initAny', taskFunction: fnc.toString() })
    return this
  }

  RenderTask(cnv) {
    this._offscreen = cnv.transferControlToOffscreen()
    this.worker.postMessage({ taskAction: 'initRender', taskCanvas: this._offscreen }, [this._offscreen])
    return this
  }

  UpdateData(data={}) {
    this.worker.postMessage({ taskAction: 'data', taskData: data })
    return this
  }

  Start(data={}) {
    this.worker.postMessage({ taskAction: 'start', taskData: data })
    return this
  }

  Terminate() {
    this.worker.terminate()
    return this.result
  }

  Result() {
    return new Promise((res, rej) => {
      if (this._offscreen) return rej(new Error('Cannot get the result of a RenderTask'))
      if (typeof this.result !== 'undefined') return res(this.result)
      const l = ev => {
        this.worker.removeEventListener('message', l)
        if (ev.data.taskAction == 'result') {
          this.result = ev.data.taskResult ?? null
          res(ev.data.taskResult)
        } else {
          rej(ev.data)
        }
      }
      this.worker.addEventListener('message', l)
    })
  }
}

