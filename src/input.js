export class Input {
  constructor(elem) {
    this.elem = elem
    this.keys = new Set()
    this.mousekeys = new Set()
    this.mouseX = 0
    this.mouseY = 0

    this.keydown = window.addEventListener('keydown', ev => {
      if (ev.repeat) return

      this.keys.add(ev.key.toLowerCase())
    })
    this.keyup = window.addEventListener('keyup', ev => {
      this.keys.delete(ev.key.toLowerCase())
    })
    this.mousemove = window.addEventListener('mousemove', ev => {
      this.mouseX = ev.clientX / this.elem.clientWidth * this.elem.width
      this.mouseY = ev.clientY / this.elem.clientHeight * this.elem.height
    })
    this.mousedown = window.addEventListener('mousedown', ev => {
      this.mousekeys.add(ev.which)
    })
    this.mouseup = window.addEventListener('mouseup', ev => {
      this.mousekeys.delete(ev.which)
    })
  }

  isKeyHeld(key) {
    return this.keys.has(key.toLowerCase())
  }

  isKeyTapped(key) {
    const v = this.keys.has(key.toLowerCase())
    if (v) this.keys.delete(key.toLowerCase())
    return v
  }

  isMouseHeld(key) {
    return this.mousekeys.has(key)
  }

  isMouseTapped(key) {
    const v = this.mousekeys.has(key)
    if (v) this.mousekeys.delete(key)
    return v
  }
}
