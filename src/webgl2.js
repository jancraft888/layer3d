import { m4, DEG2RAD, normvec, randvec3 } from './util.js'

export const VERTEX_SHADER = `#version 300 es
layout (location=0) in vec4 position;
layout (location=1) in vec3 uv;

uniform mat4 u_perspective;
uniform mat4 u_model;
uniform mat4 u_view;

out vec3 vTexCoord;

void main() {
    vTexCoord = uv;
    gl_Position = u_perspective * u_view * u_model * position;
}`

export const FRAGMENT_SHADER = `#version 300 es
precision mediump float;
precision mediump sampler3D;

uniform sampler3D pixels;

in vec3 vTexCoord;
out vec4 fragColor;

void main() {
    vec4 color = texture(pixels, vTexCoord);
    if (color.a < 0.5) discard;
    fragColor = color;
}`

export const PARTICLE_VERTEX_SHADER = `#version 300 es
layout (location=0) in vec4 pos;
layout (location=1) in vec4 color;
layout (location=2) in float size;

uniform mat4 u_perspective;
uniform mat4 u_view;

out vec4 vColor;

void main() {
    vColor = color;
    gl_Position = u_perspective * u_view * pos;
    gl_PointSize = size / gl_Position.w;
}`

export const PARTICLE_FRAGMENT_SHADER = `#version 300 es
precision mediump float;

in vec4 vColor;
out vec4 fragColor;

void main() {
    fragColor = vColor;
}`

// https://stackoverflow.com/a/11068286
function parseColor(input) {
  const div = document.createElement('div')
  document.body.append(div)
  div.style.color = input
  const m = getComputedStyle(div).color.split(',')
  div.remove()
  m[0] = m[0].substring(m[0].startsWith('rgba') ? 5 : 4)
  if (m.length != 4) m.push('1.0')
  m[3] *= 255
  return m.map(parseFloat)
}

const primitiveModels = {}
function getPrimitveModel(gl, type) {
  if (type in primitiveModels)
    return primitiveModels[type]

  if (type == 'pyramid') {
    return primitiveModels[type] = make3DModel(gl, [
      -1.0, -1.0, -1.0,
      -1.0, -1.0, 1.0,
      1.0, -1.0, -1.0,
      1.0, -1.0, 1.0,
      0.0, 1.0, 0.0
    ], [
      2, 1, 0,
      2, 3, 1,
      2, 3, 4,
      1, 0, 4,
      2, 0, 4,
      1, 3, 4
    ], [
      0.0, 0.0, 0.0,
      0.0, 1.0, 0.0,
      1.0, 0.0, 0.0,
      1.0, 1.0, 0.0,
      0.5, 0.5, 1.0
    ])
  } else if (type == 'cube') {
    return primitiveModels[type] = make3DModel(gl, [
      -1.0, -1.0, -1.0,
      -1.0, -1.0, 1.0,
      1.0, -1.0, -1.0,
      1.0, -1.0, 1.0,
      -1.0, 1.0, -1.0,
      -1.0, 1.0, 1.0,
      1.0, 1.0, -1.0,
      1.0, 1.0, 1.0,
    ], [
      2, 1, 0,
      2, 3, 1,
      1, 0, 4,
      1, 7, 3,
      2, 7, 3,
      2, 0, 4,
      2, 6, 4,
      1, 5, 4,
      2, 6, 7,
      1, 5, 7,
      6, 5, 4,
      6, 7, 5
    ], [
      0.0, 0.0, 0.0,
      0.0, 1.0, 0.0,
      1.0, 0.0, 0.0,
      1.0, 1.0, 0.0,
      0.0, 0.0, 1.0,
      0.0, 1.0, 1.0,
      1.0, 0.0, 1.0,
      1.0, 1.0, 1.0,
    ])
  } else if (type == 'plane') {
    return primitiveModels[type] = make3DModel(gl, [
      -1.0, 0.0, -1.0,
      -1.0, 0.0, 1.0,
      1.0, 0.0, -1.0,
      1.0, 0.0, 1.0
    ], [
      2, 1, 0,
      2, 3, 1
    ], [
      0.0, 0.0, 0.5,
      0.0, 1.0, 0.5,
      1.0, 0.0, 0.5,
      1.0, 1.0, 0.5,
    ])
  }
}

export class WebGL2Renderer {
  constructor(canvas) {
    this.canvas = canvas
    this.gl = canvas.getContext('webgl2')
    this.gl.enable(this.gl.DEPTH_TEST)
    this.w = canvas.width
    this.h = canvas.height
    this.camera = { x: 0, y: 5, z: 5, vangle: -35.0, hangle: 0.0, rangle: 0.0 }
    this.feature = { skyColor: [ 0, 0, 0, 0 ], fov: 90, near: 0.1, far: 1000 }

    const vs = createShader(this.gl, this.gl.VERTEX_SHADER, VERTEX_SHADER)
    const fs = createShader(this.gl, this.gl.FRAGMENT_SHADER, FRAGMENT_SHADER)
    const prg = createProgram(this.gl, vs, fs)

    const pmatUnifLoc = this.gl.getUniformLocation(prg, "u_perspective")
    const vmatUnifLoc = this.gl.getUniformLocation(prg, "u_view")
    const mmatUnifLoc = this.gl.getUniformLocation(prg, "u_model")

    this.program = prg
    this.uniforms = { pmatUnifLoc, vmatUnifLoc, mmatUnifLoc }

    window.addEventListener('resize', this._recalculate_perspective.bind(this))
    this._recalculate_perspective()

    this.canvas.style.imageRendering = 'pixelated'
  }

  _recalculate_perspective() {
    this.perspective = m4.perspective(this.feature.fov * DEG2RAD, this.canvas.clientWidth / this.canvas.clientHeight, this.feature.near, this.feature.far)
    this.gl.useProgram(this.program)
    this.gl.uniformMatrix4fv(this.uniforms.pmatUnifLoc, false, this.perspective)
  }

  render(scene) {
    this.gl.viewport(0, 0, this.canvas.width, this.canvas.height)
    this.gl.clearColor(...this.feature.skyColor)
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT)

    this.gl.useProgram(this.program)
    this.vmat = m4.identity()
    this.vmat = m4.translate(this.vmat, this.camera.x, this.camera.y, this.camera.z)
    this.vmat = m4.rotateZ(this.vmat, -this.camera.rangle * DEG2RAD)
    this.vmat = m4.rotateY(this.vmat, -this.camera.hangle * DEG2RAD)
    this.vmat = m4.rotateX(this.vmat, this.camera.vangle * DEG2RAD)
    this.vmat = m4.inverse(this.vmat)
    this.gl.uniformMatrix4fv(this.uniforms.vmatUnifLoc, false, this.vmat)

    for (const entity of scene) {
      if (entity.typ == 'layers') {
        // we have to upgrade the model to a '3d' type using makeLayeredModel() and makeTexture3D()
        entity.model = makeLayeredModel(this.gl, entity.imgs.length)
        function gettexdat() {
          const texdat = new Uint8Array(entity.imgs[0].width * entity.imgs.length * entity.imgs[0].height * 4)
          for (let i = 0; i < entity.imgs.length; i++) {
            texdat.set(entity.imgs[i].data, i * entity.imgs[0].width * entity.imgs[0].height * 4)
          }
          return texdat
        }
        entity.texture = makeTexture3D(this.gl, entity.imgs[0].width, entity.imgs[0].height, entity.imgs.length, this.gl.TEXTURE0, gettexdat())
        entity.updatetex = () => {
          this.gl.activeTexture(this.gl.TEXTURE0)
          this.gl.bindTexture(this.gl.TEXTURE_3D, entity.texture)
          this.gl.texImage3D(this.gl.TEXTURE_3D, 0, this.gl.RGBA4, entity.imgs[0].width, entity.imgs[0].height, entity.imgs.length, 0, this.gl.RGBA, this.gl.UNSIGNED_BYTE, gettexdat())
        }

        entity.typ = '3d'
      }
      if (entity.typ == 'pyramid' || entity.typ == 'plane' || entity.typ == 'cube') {
        // upgrade into a real 3d object
        entity.model = getPrimitveModel(this.gl, entity.typ)
        // use a 16x16x16 texture for the color (if a function)
        if (typeof entity.c === 'function') {
          const SLICES = entity.typ == 'plane' ? 1 : 16
          const arr = new Uint8Array(SLICES * 4)
          for (let i = 0; i < SLICES; i++) {
            const c = parseColor(entity.c(i / SLICES)).map(Math.floor)
            arr[i * 4] = c[0]
            arr[i * 4 + 1] = c[1]
            arr[i * 4 + 2] = c[2]
            arr[i * 4 + 3] = c[3]
          }
          entity.texture = makeTexture3D(this.gl, 1, 1, SLICES, this.gl.TEXTURE0, arr)
        } else {
          entity.texture = makeTexture3D(this.gl, 1, 1, 1, this.gl.TEXTURE0, new Uint8Array(parseColor(entity.c).map(Math.floor)))
        }
        entity.typ = '3d'
      }

      if (entity.typ != '3d')
        continue

      let mmat = m4.translation(entity.x, entity.y, entity.z)
      mmat = m4.rotateZ(mmat, entity.rz * DEG2RAD)
      mmat = m4.rotateY(mmat, entity.ry * DEG2RAD)
      mmat = m4.rotateX(mmat, entity.rx * DEG2RAD)
      mmat = m4.scale(mmat, entity.sx, entity.sy, entity.sz)
      this.gl.uniformMatrix4fv(this.uniforms.mmatUnifLoc, false, mmat)

      this.gl.activeTexture(this.gl.TEXTURE0)
      this.gl.bindTexture(this.gl.TEXTURE_3D, entity.texture)
      this.gl.bindVertexArray(entity.model.vao)
      this.gl.drawElements(this.gl.TRIANGLES, entity.model.count, this.gl.UNSIGNED_SHORT, 0)
      this.gl.bindVertexArray(null) // remember to clear the VAO or we may corrupt it
    }
  }
}

// Each particle system can hold up to 1024 particles
export class WebGL2ParticleSystem {
  constructor(particleRenderer) {
    this.renderer = particleRenderer
    this.emitters = []
    this.particles = []
    this.lasttime = performance.now()

    this.gl = particleRenderer.gl

    this.vao = this.gl.createVertexArray()
    this.gl.bindVertexArray(this.vao)

    this.gl.enableVertexAttribArray(0)
    this.posdata = new Float32Array(1024 * 3)
    this.posbuf = this.gl.createBuffer()
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.posbuf)
    this.gl.bufferData(this.gl.ARRAY_BUFFER, this.posdata, this.gl.DYNAMIC_DRAW)
    this.gl.vertexAttribPointer(0, 3, this.gl.FLOAT, false, 0, 0)

    this.gl.enableVertexAttribArray(1)
    this.coldata = new Float32Array(1024 * 3)
    this.colbuf = this.gl.createBuffer()
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.colbuf)
    this.gl.bufferData(this.gl.ARRAY_BUFFER, this.coldata, this.gl.DYNAMIC_DRAW)
    this.gl.vertexAttribPointer(1, 3, this.gl.FLOAT, false, 0, 0)

    this.gl.enableVertexAttribArray(2)
    this.sizdata = new Float32Array(1024)
    this.sizbuf = this.gl.createBuffer()
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.sizbuf)
    this.gl.bufferData(this.gl.ARRAY_BUFFER, this.sizdata, this.gl.DYNAMIC_DRAW)
    this.gl.vertexAttribPointer(2, 1, this.gl.FLOAT, false, 0, 0)

    this.gl.bindVertexArray(null)
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, null)
  }

  update() {
    const delta = (performance.now() - this.lasttime) / 1000
    this.lasttime = performance.now()

    for (const emitter of this.emitters) {
      emitter._acc += delta
      emitter.life -= delta
      if (emitter._acc > 1 / emitter.rate) {
        emitter._acc -= 1 / emitter.rate
        const prtcl = Object.assign({}, emitter.particle)
        const pos = randvec3()
        prtcl.x = emitter.x + pos[0] * emitter.ax
        prtcl.y = emitter.y + pos[1] * emitter.ay
        prtcl.z = emitter.z + pos[2] * emitter.az
        this.emit(prtcl, emitter.burst)
      }
    }
    this.emitters = this.emitters.filter(v => v.life > 0)

    let i = 0
    for (const prtcl of this.particles) {
      prtcl.size -= delta * prtcl.decay
      prtcl.life -= delta
      prtcl.x += prtcl.vx * delta
      prtcl.y += prtcl.vy * delta
      prtcl.z += prtcl.vz * delta
      prtcl.vy -= prtcl.gravity * delta

      this.posdata[i * 3] = prtcl.x
      this.posdata[i * 3 + 1] = prtcl.y
      this.posdata[i * 3 + 2] = prtcl.z

      this.coldata[i * 3] = prtcl.color[0]
      this.coldata[i * 3 + 1] = prtcl.color[1]
      this.coldata[i * 3 + 2] = prtcl.color[2]

      this.sizdata[i] = prtcl.size

      i++
    }
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.posbuf)
    this.gl.bufferData(this.gl.ARRAY_BUFFER, this.posdata, this.gl.DYNAMIC_DRAW)
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.colbuf)
    this.gl.bufferData(this.gl.ARRAY_BUFFER, this.coldata, this.gl.DYNAMIC_DRAW)
    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.sizbuf)
    this.gl.bufferData(this.gl.ARRAY_BUFFER, this.sizdata, this.gl.DYNAMIC_DRAW)

    this.gl.bindBuffer(this.gl.ARRAY_BUFFER, null)

    this.particles = this.particles.filter(v => v.life > 0)
  }

  emit(data={}, count=0) {
    const particle = {
      color: '#fff',
      size: 16,
      x: 0, y: 0, z: 0,
      velocity: 0,
      gravity: 16,
      life: 1,
      ...data
    }
    particle.decay = (particle.size - 1) / particle.life
    for (let i = 0; i < count; i++) {
      const prtcl = Object.assign({}, particle)
      const vdir = normvec(randvec3())
      prtcl.vx = vdir[0] * particle.velocity
      prtcl.vy = vdir[1] * particle.velocity
      prtcl.vz = vdir[2] * particle.velocity
      prtcl.color = parseColor(particle.color).slice(0, 3).map(x => x / 255)
      this.particles.push(prtcl)
    }
  }

  emitter(data={}) {
    const emitter = {
      x: 0, y: 0, z: 0,
      ax: 0, ay: 0, az: 0,
      particle: {},
      burst: 10,
      rate: .5,
      _acc: 0,
      life: Infinity,
      ...data
    }
    this.emitters.push(emitter)
  }
}

export class WebGL2ParticleRenderer {
  constructor(renderer) {
    this.renderer = renderer
    this.systems = new Set()
    this.gl = renderer.gl

    const vs = createShader(this.gl, this.gl.VERTEX_SHADER, PARTICLE_VERTEX_SHADER)
    const fs = createShader(this.gl, this.gl.FRAGMENT_SHADER, PARTICLE_FRAGMENT_SHADER)
    const prg = createProgram(this.gl, vs, fs)

    const pmatUnifLoc = this.gl.getUniformLocation(prg, "u_perspective")
    const vmatUnifLoc = this.gl.getUniformLocation(prg, "u_view")

    this.program = prg
    this.uniforms = { pmatUnifLoc, vmatUnifLoc }
  }

  addSystem(system) { this.systems.add(system) }
  removeSystem(system) { this.systems.delete(system) }

  render() {
    const { w, h, camera, gl } = this.renderer

    gl.useProgram(this.program)
    gl.uniformMatrix4fv(this.uniforms.pmatUnifLoc, false, this.renderer.perspective)
    gl.uniformMatrix4fv(this.uniforms.vmatUnifLoc, false, this.renderer.vmat)

    for (const system of this.systems) {
      gl.bindVertexArray(system.vao)
      gl.drawArrays(gl.POINTS, 0, system.particles.length)
    }
    gl.bindVertexArray(null)
  }
}

export function makeBuffer(gl, type, data, draw) {
  const buf = gl.createBuffer()
  gl.bindBuffer(type, buf)
  gl.bufferData(type, data, draw)
  gl.bindBuffer(type, null)
  return buf
}

export function makeVAO(gl, positionBuffer, uvBuffer, indexBuffer, posSize=3, uvSize=2) {
  const vao = gl.createVertexArray()
  gl.bindVertexArray(vao)
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer)

  gl.enableVertexAttribArray(0)
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer)
  gl.vertexAttribPointer(0, posSize, gl.FLOAT, false, 0, 0)

  gl.enableVertexAttribArray(1)
  gl.bindBuffer(gl.ARRAY_BUFFER, uvBuffer)
  gl.vertexAttribPointer(1, uvSize, gl.FLOAT, false, 0, 0)

  gl.bindVertexArray(null)
  gl.bindBuffer(gl.ARRAY_BUFFER, null)
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null)
  return vao
}

export function make3DModel(gl, pos, idx, uv) {
  const positionBuffer = makeBuffer(gl, gl.ARRAY_BUFFER, new Float32Array(pos), gl.STATIC_DRAW)
  const indexBuffer = makeBuffer(gl, gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(idx), gl.STATIC_DRAW)
  const uvBuffer = makeBuffer(gl, gl.ARRAY_BUFFER, new Float32Array(uv), gl.STATIC_DRAW)
  const vao = makeVAO(gl, positionBuffer, uvBuffer, indexBuffer, 3, 3)
  const count = idx.length

  return { positionBuffer, indexBuffer, uvBuffer, vao, count }
}

export function makeLayeredModel(gl, slices) {
  const pos = []
  const idx = []
  const uv = []

  for (let i = 0; i < slices; i++) {
    const h = i / slices
    pos.push(...[
      -1.0, h * 2 - 1, -1.0,
      -1.0, h * 2 - 1, 1.0,
      1.0, h * 2 - 1, -1.0,
      1.0, h * 2 - 1, 1.0
    ])
    idx.push(...[
      i * 4 + 2, i * 4 + 1, i * 4 + 0,
      i * 4 + 2, i * 4 + 3, i * 4 + 1
    ])
    uv.push(...[
      0.0, 0.0, h,
      0.0, 1.0, h,
      1.0, 0.0, h,
      1.0, 1.0, h
    ])
  }

  return make3DModel(gl, pos, idx, uv)
}

export function makeTexture3D(gl, w, h, d, slot, data) {
  const tex = gl.createTexture()
  gl.activeTexture(slot)
  gl.bindTexture(gl.TEXTURE_3D, tex)
  gl.texParameteri(gl.TEXTURE_3D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
  gl.texParameteri(gl.TEXTURE_3D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
  gl.texParameteri(gl.TEXTURE_3D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
  gl.texParameteri(gl.TEXTURE_3D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
  gl.texImage3D(gl.TEXTURE_3D, 0, gl.RGBA4, w, h, d, 0, gl.RGBA, gl.UNSIGNED_BYTE, data)
  return tex
}

export function createShader(gl, typ, src) {
  const shd = gl.createShader(typ)
  gl.shaderSource(shd, src)
  gl.compileShader(shd)
  if (gl.getShaderParameter(shd, gl.COMPILE_STATUS))
    return shd

  console.warn(gl.getShaderInfoLog(shd))
  gl.deleteShader(shd)
}

export function createProgram(gl, vs, fs) {
  const prg = gl.createProgram()
  gl.attachShader(prg, vs)
  gl.attachShader(prg, fs)
  gl.linkProgram(prg)
  if (gl.getProgramParameter(prg, gl.LINK_STATUS))
    return prg

  console.warn(gl.getProgramInfoLog(prg))
  gl.deleteProgram(prg)
}
