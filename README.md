![layer3d Banner](./assets/banner.png)

# layer3d

**layer3d** is a 3D Engine built with a retro-style 2D layer volumetric renderer.

## 3D Models

**3D models** are represented as 2D slices. The engine provides fast-to-render primitives and functions to build models on the fly. On top of this, there are functions for importing and exporting layered PNG files *(.lmd)* `WIP`.

## Web Workers

An important aspect of **layer3d** is that the engine allows running it fully on Web Workers. This is important because it allows to separate rendering from logic and data loading. Asynchronous tasks are simple to start and get an answer from.

## WebGL2 Support

New addition to the engine! WebGL2 has a real perspective, performs better, and will be the main point of development from now on. Slowly we will port *all* features to WebGL2 and remove the default engine entirely (don't worry, `old` branch will be made before the merge).
